abstract class NullUtil {
  static void assertStrNE(String it) {
    assertNN(it);
    if (it.isEmpty) throw ArgumentError(it);
  }

  static void assertNN(it) {
    if (it == null) throw ArgumentError(it);
  }
}
