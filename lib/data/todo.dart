class Todo {
  String id;
  String text;
  bool completed = false;
  final DateTime dateCreated = DateTime.now();

  Todo(this.id, this.text);
}
