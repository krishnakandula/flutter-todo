import 'todo.dart';
import 'todo_repository.dart';
import 'dart:collection';

class SimpleTodoRepository implements TodoRepository {
  final Map _todoMap = HashMap<String, Todo>();

  @override
  bool addTodo(Todo todo) {
    if (todo is! Todo) throw ArgumentError(todo);
    if (todo.id is! String || todo.id.isEmpty) throw ArgumentError(todo.id);

    _todoMap.putIfAbsent(todo.id, () {
      return todo;
    });
    return false;
  }

  @override
  List<Todo> getAllTodos() => _todoMap.values.toList();

  @override
  Todo getTodo(String id) {
    if (id is! String || id.isEmpty) throw ArgumentError(id);
    return _todoMap[id];
  }

  @override
  bool removeTodo(String id) {
    if (id is! String || id.isEmpty) throw ArgumentError(id);
    if (!_todoMap.containsKey(id)) return false;
    _todoMap.remove(id);
    return true;
  }
}
