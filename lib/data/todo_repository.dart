import 'todo.dart';

abstract class TodoRepository {

  Todo getTodo(String id);

  List<Todo> getAllTodos();

  bool addTodo(Todo todo);

  bool removeTodo(String id);
}
