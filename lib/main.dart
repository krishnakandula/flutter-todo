import 'package:flutter/material.dart';
import 'data/simple_todo_repository.dart';
import 'data/todo_repository.dart';
import 'data/todo.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: TodoList(),
    );
  }
}

class TodoList extends StatefulWidget {
  @override
  State createState() => TodoListState(SimpleTodoRepository());
}

class TodoListState extends State<TodoList> {
  final TodoRepository _todoRepo;

  TodoListState(this._todoRepo);

  @override
  Widget build(BuildContext context) {
    void _addTodo() {
      setState(() {
        _todoRepo.addTodo(Todo("${Random().nextInt(10000)}", "hi"));
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Flutter Todo',
        ),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: _todoRepo.getAllTodos().length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(
              _todoRepo.getAllTodos()[index].text,
              textScaleFactor: 1.3,
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addTodo,
      ),
    );
  }
}
